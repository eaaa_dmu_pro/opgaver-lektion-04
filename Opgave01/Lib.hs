module Lib
(
    unitSphereVolume
    , unitSphereArea
) 
where

import qualified Geometry.Sphere as Sphere
unitSphereArea = Sphere.area 1
unitSphereVolume = Sphere.volume 1
