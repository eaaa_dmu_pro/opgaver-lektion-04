module Geometry.Pyramid
(
    area
    , volume
)
where

area height length = slength * length / 2 + length * length
    where slength = sqrt ((length/2)^2 + height * height)
volume height length = height * length * length / 3