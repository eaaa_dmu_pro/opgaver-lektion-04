module Geometry.Cylinder
(
    area
    , volume
)
where

area radius height = 2 * pi * radius * radius + height * 2 * pi * radius
volume radius height = height * pi * radius * radius 