module Lib
    ( hello
    ) where

import List
import Prelude hiding (head, tail)
hello = Cons 'H' (Cons 'e' (Cons 'l' (Cons 'l' (Cons 'o' (Empty)))))
empty = Empty
