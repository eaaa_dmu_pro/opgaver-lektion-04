module List (
    List(..)
    , head
    , tail
)
where

import Prelude hiding (head, tail)

data List a = Empty | Cons a (List a) deriving (Show)

head :: List a -> a
head Empty = error "empty list"
head (Cons a ( _ )) = a

tail :: List a -> List a
tail Empty = error "empty list"
tail (Cons _ Empty) = Empty
tail (Cons _ (Cons a b)) = Cons a b