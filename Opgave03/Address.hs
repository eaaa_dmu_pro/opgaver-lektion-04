module Address (
    Address (..)
    , showAddress
)
where 

data Address = Address { streetname :: String
                       , streetnumber :: Int
                       , floor :: Maybe String
                       , postalCode :: Int
                       , city :: String
                       }

showAddress :: Address -> String
showAddress (Address streetname streetnumber (Just floor) postalCode city) = 
    streetname ++ " " ++ (show streetnumber) ++ " " ++ floor ++ "\n" ++ (show postalCode) ++ " " ++ city 
showAddress (Address streetname streetnumber Nothing postalCode city) = 
        streetname ++ " " ++ (show streetnumber) ++ "\n" ++ (show postalCode) ++ " " ++ city 
    