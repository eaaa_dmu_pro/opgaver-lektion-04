module Lib
( 
    myOffice
    , pRaadhus
    , pMyOffice
) 
where

import Address

myOffice = Address "Soenderhoej" 30 (Just "A2.23") 8260 "Viby J" 
raadhus = Address "Raadhuspladsen" 2 Nothing 8000 "Aarhus"

pMyOffice :: IO ()
pMyOffice = putStrLn (showAddress myOffice)

pRaadhus :: IO ()
pRaadhus = putStrLn (showAddress raadhus)

