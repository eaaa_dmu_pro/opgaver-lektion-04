module Lib
( 
    unitCircle
    , triangle1
    , triangle2
    ) where

import Shape
unitCircle = Circle (Point 0 0 ) 1

triangle1 = Triangle (Point 15 15) (Point 23 30) (Point 50 25)
triangle2 = Triangle (Point (-8) 7) (Point 5 6) (Point (-3) (-5))