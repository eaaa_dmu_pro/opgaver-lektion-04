module Shape 
(
    Point (Point)
    , Shape (..)
    , surface
    , nudge
    , circumference
)
where

data Point = Point Float Float deriving (Show)  
data Shape = Circle Point Float | Rectangle Point Point | Triangle Point Point Point deriving (Show)  

surface :: Shape -> Float  
surface (Circle _ r) = pi * r ^ 2  
surface (Rectangle (Point x1 y1) (Point x2 y2)) = (abs $ x2 - x1) * (abs $ y2 - y1)
surface (Triangle (Point x0 y0) (Point x1 y1) (Point x2 y2)) = abs ((x0 * (y1 - y2) + x1 * (y2 - y0) + x2 * (y0 - y1)) / 2)  

nudge :: Shape -> Float -> Float -> Shape  
nudge (Circle (Point x y) r) a b = Circle (Point (x+a) (y+b)) r  
nudge (Rectangle (Point x1 y1) (Point x2 y2)) a b = Rectangle (Point (x1+a) (y1+b)) (Point (x2+a) (y2+b))  
nudge (Triangle (Point x0 y0) (Point x1 y1) (Point x2 y2)) a b = Triangle (Point (x0+a) (y0+b)) (Point (x1+a) (y1+b)) (Point (x2+a) (y2+b))

circumference :: Shape -> Float
circumference (Circle _ r) = 2 * pi * r
circumference (Rectangle (Point x1 y1) (Point x2 y2)) = 2 * (height + width)
    where height = abs (y2 - y1)
          width = abs (x2 - x1)
circumference (Triangle p1 p2 p3) = (sideLength p1 p2) + (sideLength p2 p3) + (sideLength p3 p1) 

sideLength (Point x1 y1) (Point x2 y2)= sqrt ((x2 -x1)^2 + (y2 -y1)^2)