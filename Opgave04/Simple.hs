module Simple (
    Simple (One)
    , Number (..)
    , addNumbers
    , subtractNumbers
    , multiplyNumbers
)
where 

import Data.List
data Simple = One deriving (Show)
data Number = Number [Simple] deriving (Show)

addNumbers :: Number -> Number -> Number
addNumbers (Number a) (Number b) = Number (a ++ b)

subtractNumbers :: Number -> Number -> Number
subtractNumbers (Number a) (Number []) = Number a
subtractNumbers (Number []) (Number a) = error "Can't handle negative numbers"
subtractNumbers (Number a) (Number b) = subtractNumbers (Number (tail a)) (Number (tail b))

multiplyNumbers :: Number -> Number -> Number
multiplyNumbers (Number []) _ = Number []
multiplyNumbers _ (Number []) = Number []
multiplyNumbers (Number a) (Number [One]) = Number a
multiplyNumbers (Number a) (Number b) = addNumbers (Number a) (multiplyNumbers (Number a) (Number (tail b)))

 